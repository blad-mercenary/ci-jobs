# CI Jobs

## Install

### install

Will add both `install-dev` and `install-prod`, who respectively install dependencies with and without dev dependencies.

## Build

### compile

Compile typescript files.

Dependencies: 

- CI job `install-dev`
- npm command `compile`

## Test

### lint

Dependencies:
 
- CI job `install-dev`

### test

Dependencies:
 
- CI job `install-dev`

## Publish

### npm

Dependencies:
 
- Var `NPM_TOKEN` with valid npm access token
